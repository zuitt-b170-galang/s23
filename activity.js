db.users.insertOne({
    roomName: "single",
    capacity: 2,
    price:1000,
    description:"A simple room with all the basic necesities."
    roomsAvailable:10,
    isAvailable:false
});

db.users.insertMany([
    {
        roomName: "double",
        capacity: 3,
        price:2000,
        description:"A room fit for a asmall family going on a vacation."
        roomsAvailable:5,
        isAvailable:false
    },
    {
        roomName: "double",
        capacity: 3,
        price:2000,
        description:"A room fit for a asmall family going on a vacation."
        roomsAvailable:5,
        isAvailable:false
    },
    {
        roomName: "double",
        capacity: 3,
        price:2000,
        description:"A room fit for a asmall family going on a vacation."
        roomsAvailable:5,
        isAvailable:false
    }  
]);

db.users.insertMany([
    {
        roomName: "queen",
        capacity: 4,
        price:4000,
        description:"A room with a queen sized bed perfect for a simple getaway."
        roomsAvailable:15,
        isAvailable:false
    },
    {
        roomName: "queen",
        capacity: 4,
        price:4000,
        description:"A room with a queen sized bed perfect for a simple getaway."
        roomsAvailable:15,
        isAvailable:false
    },
    {
        roomName: "queen",
        capacity: 4,
        price:4000,
        description:"A room with a queen sized bed perfect for a simple getaway."
        roomsAvailable:15,
        isAvailable:false
    }  
]);


db.users.find({ roomName: "double" });

db.users.updateOne(
    {
        roomsAvailable: 15
    },
    {
        $set: {
            roomsAvailable: 0
        }
    }
)

db.users.deleteMany({roomsAvailable: 0})